//#include <FIR.h>
//#include <WiFi.h>
//#include <ESPmDNS.h>
//#include <WiFiUdp.h>
//#include <ArduinoOTA.h>
//#include "RemoteDebug.h"        //https://github.com/JoaoLopesF/RemoteDebug
//#include <DNSServer.h>
//
//const char* ssid = "FRITZ!Box 6490 Cable";
//const char* password = "aw@kJeQR2z*0qWQ*";
//
//#define cmADCnum 2
//#define cmADC0 36
//#define cmADC1 39
//#define cmADCT0 13
//
//#define cmT0 5
//#define cmT1 4
//
//#define cmR0 1
//#define cmR1 0
///*
//
//FIR filter designed with
//http://t-filter.appspot.com
//
//sampling frequency: 2000 Hz
//
// * 0 Hz - 100 Hz
//  gain = 1
//  desired ripple = 0.2 dB
//  actual ripple = 0.12280091964277762 dB
//
// * 500 Hz - 1000 Hz
//  gain = 0
//  desired attenuation = -40 dB
//  actual attenuation = -41.60823639502218 dB
//
// */
//
//#define FILTER0_TAP_NUM 11
//
//static float filter_taps0[FILTER0_TAP_NUM] = {
//    -0.018141481600939164,
//    -0.025442009826572312,
//    0.016072017836200107,
//    0.12484488964753394,
//    0.2517592430598069,
//    0.30888355319745125,
//    0.2517592430598069,
//    0.12484488964753394,
//    0.016072017836200107,
//    -0.025442009826572312,
//    -0.018141481600939164
//};
//
//
///*
//
//FIR filter designed with
//http://t-filter.appspot.com
//
//sampling frequency: 4000 Hz
//
// * 0 Hz - 100 Hz
//  gain = 1
//  desired ripple = 0.1 dB
//  actual ripple = 0.06984807175937763 dB
//
// * 200 Hz - 2000 Hz
//  gain = 0
//  desired attenuation = -60 dB
//  actual attenuation = -60.47006911865782 dB
//
// */
//
//#define FILTER1_TAP_NUM 123
//
//static float filter_taps1[FILTER1_TAP_NUM] = {
//    0.0005712573653134118,
//    0.00020100662203341487,
//    0.00020261082496380675,
//    0.00017609490301880503,
//    0.00011622259299568482,
//    0.000019298865021987488,
//    -0.00011630358133696323,
//    -0.0002891423020836187,
//    -0.0004938584542678545,
//    -0.0007209569387280336,
//    -0.0009575403479472346,
//    -0.0011880945714478153,
//    -0.0013935375745849545,
//    -0.0015514534042331762,
//    -0.0016427280858269974,
//    -0.0016450998351957595,
//    -0.0015417757723362622,
//    -0.0013193376035187869,
//    -0.0009707041429314402,
//    -0.0004964909600005909,
//    0.00009363361898845107,
//    0.0007802293174306576,
//    0.001534307009113641,
//    0.0023177655337870188,
//    0.003084107886413631,
//    0.0037809793370281314,
//    0.0043529388952452,
//    0.004743239456565276,
//    0.004899583751637658,
//    0.00477617055571386,
//    0.004338568163946595,
//    0.003567044721500874,
//    0.0024599071624817046,
//    0.0010359802727220773,
//    -0.0006640267775891735,
//    -0.0025768658806053883,
//    -0.004617587374363017,
//    -0.006682052844950978,
//    -0.008649942334306247,
//    -0.010389387801403296,
//    -0.011763363994930732,
//    -0.012635229376587542,
//    -0.012876516785688434,
//    -0.012373671343858706,
//    -0.011035097836025734,
//    -0.00879740667878461,
//    -0.00563058658416249,
//    -0.0015421622245693364,
//    0.0034207934262311997,
//    0.00917126206934291,
//    0.015583472812384407,
//    0.02249674226301623,
//    0.029720800577428746,
//    0.03704236700849318,
//    0.04423403918717245,
//    0.051063170700944516,
//    0.05730185179536818,
//    0.06273671160462518,
//    0.0671782531921368,
//    0.07046952925391589,
//    0.07249286215779831,
//    0.07317548046215731,
//    0.07249286215779831,
//    0.07046952925391589,
//    0.0671782531921368,
//    0.06273671160462518,
//    0.05730185179536818,
//    0.051063170700944516,
//    0.04423403918717245,
//    0.03704236700849318,
//    0.029720800577428746,
//    0.02249674226301623,
//    0.015583472812384407,
//    0.00917126206934291,
//    0.0034207934262311997,
//    -0.0015421622245693364,
//    -0.00563058658416249,
//    -0.00879740667878461,
//    -0.011035097836025734,
//    -0.012373671343858706,
//    -0.012876516785688434,
//    -0.012635229376587542,
//    -0.011763363994930732,
//    -0.010389387801403296,
//    -0.008649942334306247,
//    -0.006682052844950978,
//    -0.004617587374363017,
//    -0.0025768658806053883,
//    -0.0006640267775891735,
//    0.0010359802727220773,
//    0.0024599071624817046,
//    0.003567044721500874,
//    0.004338568163946595,
//    0.00477617055571386,
//    0.004899583751637658,
//    0.004743239456565276,
//    0.0043529388952452,
//    0.0037809793370281314,
//    0.003084107886413631,
//    0.0023177655337870188,
//    0.001534307009113641,
//    0.0007802293174306576,
//    0.00009363361898845107,
//    -0.0004964909600005909,
//    -0.0009707041429314402,
//    -0.0013193376035187869,
//    -0.0015417757723362622,
//    -0.0016450998351957595,
//    -0.0016427280858269974,
//    -0.0015514534042331762,
//    -0.0013935375745849545,
//    -0.0011880945714478153,
//    -0.0009575403479472346,
//    -0.0007209569387280336,
//    -0.0004938584542678545,
//    -0.0002891423020836187,
//    -0.00011630358133696323,
//    0.000019298865021987488,
//    0.00011622259299568482,
//    0.00017609490301880503,
//    0.00020261082496380675,
//    0.00020100662203341487,
//    0.0005712573653134118
//};
//
//#define HOST_NAME "remotedebug-sample"
//
////Timer Variables,
//hw_timer_t * timer = NULL;
//void IRAM_ATTR cm_take_sample();
//
////Buffer RMS Calc
//const int ci_cm_sqBuff_size = 640;
//int i_cm_sqBuff[cmADCnum][ci_cm_sqBuff_size];
//float i_cm_sqBuff_Offset[cmADCnum];
//int i_cm_sqBuffCounter;
//const int ci_cm_windowSize = 4;
//
//RemoteDebug Debug;
//
//float rmsmax;
//
//FIR<float, FILTER1_TAP_NUM> fir_lp0;
//FIR<float, FILTER1_TAP_NUM> fir_lp1;
//
//
//int i_cm_calib[cmADCnum];
//
//const int ci_cm_calibBuff_size = 1024;
//
//void setup() {
//    Serial.begin(115200);
//    rdebugVln("Booting");
//    WiFi.mode(WIFI_STA);
//    WiFi.begin(ssid, password);
//    while (WiFi.waitForConnectResult() != WL_CONNECTED) {
//        rdebugVln("Connection Failed! Rebooting...");
//        delay(5000);
//        ESP.restart();
//    }
//    cm_setup_debug();
//    cm_setup_ota();
//    rdebugVln("Ready");
//    rdebugV("IP address: ");
//    Serial.println(WiFi.localIP());
//    cm_setup_out();
//    cm_setup_adc();
//}
//
//void cm_setup_debug(){
//   
//    String hostNameWifi = HOST_NAME;
//    hostNameWifi.concat(".local");
//    
//    if (MDNS.begin(HOST_NAME)) {
//        rdebugV("* MDNS responder started. Hostname -> ");
//        rdebugVln(HOST_NAME);
//    }
//
//    MDNS.addService("telnet", "tcp", 23);
//    Debug.begin(HOST_NAME); // Initiaze the telnet server
//
//    Debug.setResetCmdEnabled(true); // Enable the reset command
//    rdebugVln("Testmessage: Hello World!");
//    rdebugVln("* Arduino RemoteDebug Library");
//    rdebugVln("*");
//    rdebugV("* WiFI connected. IP address: ");
//    //rdebugVln(WiFi.localIP());
//    rdebugVln("*");
//    rdebugVln("* Please use the telnet client (telnet for Mac/Unix or putty and others for Windows)");
//    rdebugVln("*");
//    rdebugVln("* This sample will send messages of debug in all levels.");
//    rdebugVln("*");
//    rdebugVln("* Please try change debug level in telnet, to see how it works");
//    rdebugVln("*");
//}
//
//void cm_setup_ota() {
//    ArduinoOTA
//        .onStart([]() {
//            timerAlarmDisable(timer);
//            String type;
//            if (ArduinoOTA.getCommand() == U_FLASH)
//                    type = "sketch";
//            else // U_SPIFFS
//                type = "filesystem";
//
//                    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
//                    rdebugIln("Start updating %s", type);
//            })
//    .onEnd([]() {
//        rdebugIln("\nEnd");
//    })
//    .onProgress([](unsigned int progress, unsigned int total) {
//        rdebugV("Progress: %u%%\r", (progress / (total / 100)));
//    })
//    .onError([](ota_error_t error) {
//        rdebugI("Error[%u]: ", error);
//        if (error == OTA_AUTH_ERROR) rdebugIln("Auth Failed");
//        else if (error == OTA_BEGIN_ERROR) rdebugIln("Begin Failed");
//        else if (error == OTA_CONNECT_ERROR) rdebugIln("Connect Failed");
//        else if (error == OTA_RECEIVE_ERROR) rdebugIln("Receive Failed");
//        else if (error == OTA_END_ERROR) rdebugIln("End Failed");
//        });
//
//    ArduinoOTA.begin();
//}
//
//void cm_setup_out() {
//    pinMode(cmT0, OUTPUT);
//    pinMode(cmT1, OUTPUT);
//    pinMode(cmR0, OUTPUT);
//    pinMode(cmR1, OUTPUT);
//
//    digitalWrite(cmT0, HIGH);
//    digitalWrite(cmT1, HIGH);
//    digitalWrite(cmR0, LOW);
//    digitalWrite(cmR1, LOW);
//}
//
//void cm_setup_adc() {
//
//    adcAttachPin(cmADC0);
//    adcAttachPin(cmADC1);
//    adcAttachPin(cmADCT0);
//    analogSetPinAttenuation(cmADC0, ADC_11db);
//    analogSetPinAttenuation(cmADC1, ADC_11db);
//    analogSetPinAttenuation(cmADCT0, ADC_11db);
//    analogReadResolution(12);
//    i_cm_sqBuffCounter = 0;
//
//    timer = timerBegin(0, 80, true);
//    timerAttachInterrupt(timer, &cm_take_sample, true);
//    timerAlarmWrite(timer, 250, true);
//    timerAlarmEnable(timer);
//
//    fir_lp0.setFilterCoeffs(filter_taps1);
//    fir_lp0.getGain();
//    fir_lp1.setFilterCoeffs(filter_taps1);
//    fir_lp1.getGain();
//    cm_calib_adc();
//    rmsmax = 0;
//}
//
//void cm_calib_adc() {
//    delay(1000);
//    for (int i = 0; i < cmADCnum; i++) i_cm_calib[i] = 0;
//    //    for (int i = 0; i < ci_cm_calibBuff_size; i++) {
//    //        adcStart(cmADC0);
//    //        i_cm_calib[0] += adcEnd(cmADC0);
//    //        adcStart(cmADC1);
//    //        i_cm_calib[1] += adcEnd(cmADC1);
//    //        //rdebugV("Cal_Meas: %u, %u\n", i_cm_calib[0],  i_cm_calib[1]);
//    //    }
//    for (int channel = 0; channel < cmADCnum; channel++) {
//        for (int i = 0; i < ci_cm_sqBuff_size; i++) {
//            i_cm_calib[channel] += i_cm_sqBuff[channel][i];
//        }
//    }
//    for (int i = 0; i < cmADCnum; i++) i_cm_calib[i] /= ci_cm_sqBuff_size;
//    rdebugI("Cal_Meas: %f, %f\n", i_cm_calib[0]* 1.95 / 4096.0, i_cm_calib[1]*1.95 / 4096.0);
//}
//
//void loop() {
//
//    //rdebugV("%i ADC: %f (%f), %f (%f)\n", micros(),  i_cm_sqBuff[0][i_cm_sqBuffCounter] * 3.6 / 4096.0, cm_rms_calc(0),i_cm_sqBuff[1][i_cm_sqBuffCounter] * 1.95 / 4096.0, cm_rms_calc(1));
//
////    if (Serial.available()) { // If anything comes in Serial (USB),
////        char x = Serial.read(); // read it and send it out Serial1 (pins 0 & 1)
////        if (x == 't') {
////            rdebugV("%u \n", i_cm_calib[0]);
////            for (int i = 0; i < ci_cm_sqBuff_size; i++) {
////                rdebugV("%u, %f, %f \n", i_cm_sqBuff[0][i], fir_lp0.processReading(i_cm_sqBuff[0][i] - i_cm_calib[0]), fir_lp1.processReading(i_cm_sqBuff[0][i] - i_cm_calib[0]));
////            }
////        }
////    }
//    int t = analogRead(cmADCT0);
//    float temp = 4.1 * t / 4096.0;
//    temp = 1212576.05 / (log(2 / (3 * temp) - 2 / 15)*298.15 + 4067) - 273.15;
//    float temp0 = cm_rms_calc(0);
//    float temp1 = cm_rms_calc(1);
//    //if (temp0>rmsmax) rmsmax = temp0;
//    rdebugV("%i ADC: t= %f, %f (%f), %f (%f)\n", micros(), temp, i_cm_sqBuff[0][i_cm_sqBuffCounter] * 3.6 / 4096.0, temp0, i_cm_sqBuff[1][i_cm_sqBuffCounter] * 3.6 / 4096.0, temp1);
//    ArduinoOTA.handle();
//    Debug.handle();
//}
//
//void IRAM_ATTR cm_take_sample() {
//    i_cm_sqBuff[0][i_cm_sqBuffCounter] = analogRead(cmADC0);
//    i_cm_sqBuff[1][i_cm_sqBuffCounter] = analogRead(cmADC1);
//    if (i_cm_sqBuffCounter < ci_cm_sqBuff_size) i_cm_sqBuffCounter++;
//    else i_cm_sqBuffCounter = 0;
//}
//
//float cm_rms_calc(int channel) {
//
//    //i_cm_sqBuff[channel][i_cm_sqBuffCounter] = fir_lp.processReading(value);
//    //calcuate new RMS value use mean of size ci_winduwSize
//    float rms = 0;
//    for (int i = 0; i < ci_cm_sqBuff_size; i++) {
//        float temp = 0;
//        //        for (int ii = 0; ii < ci_cm_windowSize; ii++) {
//        //            temp += i_cm_sqBuff[channel][i + ii];
//        //        }
//        //        temp /= ci_cm_windowSize;
//        if (channel == 0) temp = fir_lp0.processReading(i_cm_sqBuff[channel][i]) - i_cm_calib[0]; //fir_lp0.processReading(i_cm_sqBuff[channel][i]);
//        if (channel == 1) temp = fir_lp1.processReading(i_cm_sqBuff[channel][i]) - i_cm_calib[1]; //fir_lp1.processReading(i_cm_sqBuff[channel][i]);
//        rms += temp * temp;
//    }
//    rms /= ci_cm_sqBuff_size;
//    rms = sqrt(rms);
//    rms = rms * 8.08 * 3.9 / 4096.0 - 0.021; // - i_cm_sqBuff_Offset[channel];
//    return rms * 240.0;
//}

/*TODO HARDWARE:
 * Impedanzen variiren
 * Höheren Spannungsbereich probieren
 * Analog Filter einbauen @ 80kHz oder obere Sampling Frequenz
 * FIR Filter: http://t-filter.engineerjs.com/
 */

////////
// Libraries Arduino
//
// Library: Remote debug - debug over telnet - for Esp8266 (NodeMCU) or ESP32
// Author: Joao Lopes
//
// Attention: This library is only for help development. Please not use this in production
//
// First sample to show how to use it - advanced one
//
// Example of use:
//
//        if (Debug.isActive(Debug.<level>)) { // <--- This is very important to reduce overheads and work of debug levels
//            Debug.printf("bla bla bla: %d %s\n", number, str);
//            Debug.println("bla bla bla");
//        }
//
// Or short way (prefered if only one debug at time)
//
//		debugAln("This is a any (always showed) - var %d", var);
//
//		debugV("This is a verbose - var %d", var);
//		debugD("This is a debug - var %d", var);
//		debugI("This is a information - var %d", var);
//		debugW("This is a warning - var %d", var);
//		debugE("This is a error - var %d", var);
//
//		debugV("This is println");
//
//
///////

// Libraries

#define ESP32 1
#define USE_ARDUINO_OTA 1

#include <WiFi.h>
#include <FIR.h>
#ifdef USE_ARDUINO_OTA
#include <ArduinoOTA.h>
#endif

// Production

//#define PRODUCTION true

// HTTP Web server - discomment if you need this
// ESP8266WebServer HTTPServer(80);

// Remote debug over telnet - not recommended for production, only for development
// I put it to show how to do code clean to development and production

#ifndef PRODUCTION // Not in PRODUCTION

#include "RemoteDebug.h"        //https://github.com/JoaoLopesF/RemoteDebug

RemoteDebug Debug;

#endif

// Host name

#define HOST_NAME "rem-debug" // PLEASE CHANGE IT

// Time

uint32_t mLastTime = 0;
uint32_t mTimeSeconds = 0;

// Buildin Led ON ?

////// Setup

const char* ssid = "FRITZ!Box 6490 Cable";
const char* password = "aw@kJeQR2z*0qWQ*";

#define cmADCnum 2
#define cmADC0 36
#define cmADC1 39
#define cmADCT0 13

#define cmT0 5
#define cmT1 4

#define cmR0 1
#define cmR1 0
/*

FIR filter designed with
http://t-filter.appspot.com

sampling frequency: 2000 Hz

 * 0 Hz - 100 Hz
  gain = 1
  desired ripple = 0.2 dB
  actual ripple = 0.12280091964277762 dB

 * 500 Hz - 1000 Hz
  gain = 0
  desired attenuation = -40 dB
  actual attenuation = -41.60823639502218 dB

 */

#define FILTER0_TAP_NUM 11

static float filter_taps0[FILTER0_TAP_NUM] = {
    -0.018141481600939164,
    -0.025442009826572312,
    0.016072017836200107,
    0.12484488964753394,
    0.2517592430598069,
    0.30888355319745125,
    0.2517592430598069,
    0.12484488964753394,
    0.016072017836200107,
    -0.025442009826572312,
    -0.018141481600939164
};


/*

FIR filter designed with
http://t-filter.appspot.com

sampling frequency: 4000 Hz

 * 0 Hz - 100 Hz
  gain = 1
  desired ripple = 0.1 dB
  actual ripple = 0.06984807175937763 dB

 * 200 Hz - 2000 Hz
  gain = 0
  desired attenuation = -60 dB
  actual attenuation = -60.47006911865782 dB

 */

#define FILTER1_TAP_NUM 123

static float filter_taps1[FILTER1_TAP_NUM] = {
    0.0005712573653134118,
    0.00020100662203341487,
    0.00020261082496380675,
    0.00017609490301880503,
    0.00011622259299568482,
    0.000019298865021987488,
    -0.00011630358133696323,
    -0.0002891423020836187,
    -0.0004938584542678545,
    -0.0007209569387280336,
    -0.0009575403479472346,
    -0.0011880945714478153,
    -0.0013935375745849545,
    -0.0015514534042331762,
    -0.0016427280858269974,
    -0.0016450998351957595,
    -0.0015417757723362622,
    -0.0013193376035187869,
    -0.0009707041429314402,
    -0.0004964909600005909,
    0.00009363361898845107,
    0.0007802293174306576,
    0.001534307009113641,
    0.0023177655337870188,
    0.003084107886413631,
    0.0037809793370281314,
    0.0043529388952452,
    0.004743239456565276,
    0.004899583751637658,
    0.00477617055571386,
    0.004338568163946595,
    0.003567044721500874,
    0.0024599071624817046,
    0.0010359802727220773,
    -0.0006640267775891735,
    -0.0025768658806053883,
    -0.004617587374363017,
    -0.006682052844950978,
    -0.008649942334306247,
    -0.010389387801403296,
    -0.011763363994930732,
    -0.012635229376587542,
    -0.012876516785688434,
    -0.012373671343858706,
    -0.011035097836025734,
    -0.00879740667878461,
    -0.00563058658416249,
    -0.0015421622245693364,
    0.0034207934262311997,
    0.00917126206934291,
    0.015583472812384407,
    0.02249674226301623,
    0.029720800577428746,
    0.03704236700849318,
    0.04423403918717245,
    0.051063170700944516,
    0.05730185179536818,
    0.06273671160462518,
    0.0671782531921368,
    0.07046952925391589,
    0.07249286215779831,
    0.07317548046215731,
    0.07249286215779831,
    0.07046952925391589,
    0.0671782531921368,
    0.06273671160462518,
    0.05730185179536818,
    0.051063170700944516,
    0.04423403918717245,
    0.03704236700849318,
    0.029720800577428746,
    0.02249674226301623,
    0.015583472812384407,
    0.00917126206934291,
    0.0034207934262311997,
    -0.0015421622245693364,
    -0.00563058658416249,
    -0.00879740667878461,
    -0.011035097836025734,
    -0.012373671343858706,
    -0.012876516785688434,
    -0.012635229376587542,
    -0.011763363994930732,
    -0.010389387801403296,
    -0.008649942334306247,
    -0.006682052844950978,
    -0.004617587374363017,
    -0.0025768658806053883,
    -0.0006640267775891735,
    0.0010359802727220773,
    0.0024599071624817046,
    0.003567044721500874,
    0.004338568163946595,
    0.00477617055571386,
    0.004899583751637658,
    0.004743239456565276,
    0.0043529388952452,
    0.0037809793370281314,
    0.003084107886413631,
    0.0023177655337870188,
    0.001534307009113641,
    0.0007802293174306576,
    0.00009363361898845107,
    -0.0004964909600005909,
    -0.0009707041429314402,
    -0.0013193376035187869,
    -0.0015417757723362622,
    -0.0016450998351957595,
    -0.0016427280858269974,
    -0.0015514534042331762,
    -0.0013935375745849545,
    -0.0011880945714478153,
    -0.0009575403479472346,
    -0.0007209569387280336,
    -0.0004938584542678545,
    -0.0002891423020836187,
    -0.00011630358133696323,
    0.000019298865021987488,
    0.00011622259299568482,
    0.00017609490301880503,
    0.00020261082496380675,
    0.00020100662203341487,
    0.0005712573653134118
};

#define HOST_NAME "remotedebug-sample"

//Timer Variables,
hw_timer_t * timer = NULL;
void IRAM_ATTR cm_take_sample();

//Buffer RMS Calc
static const int ci_cm_sqBuff_size = 320;
static volatile int i_cm_sqBuff[cmADCnum][ci_cm_sqBuff_size];
static volatile float i_cm_sqBuff_Offset[cmADCnum];
static volatile int i_cm_sqBuffCounter;
const int ci_cm_windowSize = 4;

float rmsmax;

FIR<float, FILTER1_TAP_NUM> fir_lp0;
FIR<float, FILTER1_TAP_NUM> fir_lp1;


int i_cm_calib[cmADCnum];

const int ci_cm_calibBuff_size = 256;

void setup() {

	// Initialize the Serial (educattional use only, not need in production)

	Serial.begin(115200);

	// Connect WiFi

	connectWiFi();

	// Host name of WiFi

#ifdef USE_ARDUINO_OTA
	// Update over air (OTA)

	initializeOTA();
#endif

	// Register host name in mDNS

	// Initialize the telnet server of RemoteDebug

#ifndef PRODUCTION // Not in PRODUCTION

	Debug.begin(HOST_NAME); // Initiaze the telnet server

	//Debug.setPassword("r3m0t0."); // Password on telnet connection ?

	Debug.setResetCmdEnabled(true); // Enable the reset command

	//Debug.showDebugLevel(false); // To not show debug levels
	//Debug.showTime(true); // To show time
	//Debug.showProfiler(true); // To show profiler - time between messages of Debug
	// Good to "begin ...." and "end ...." messages

	Debug.showProfiler(true); // Profiler
	Debug.showColors(true); // Colors

	// Debug.setSerialEnabled(true); // if you wants serial echo - only recommended if ESP8266 is plugged in USB

	String helpCmd = "bench1 - Benchmark 1\n";
	helpCmd.concat("bench2 - Benchmark 2");

	Debug.setHelpProjectsCmds(helpCmd);
	Debug.setCallBackProjectCmds(&processCmdRemoteDebug);

	// This sample

	Serial.println("* Arduino RemoteDebug Library");
	Serial.println("*");
	Serial.print("* WiFI connected. IP address: ");
	Serial.println(WiFi.localIP());
	Serial.println("*");
	Serial.println(
			"* Please use the telnet client (telnet for Mac/Unix or putty and others for Windows)");
	Serial.println("*");
	Serial.println("* This sample will send messages of debug in all levels.");
	Serial.println("*");
	Serial.println(
			"* Please try change debug level in telnet, to see how it works");
	Serial.println("*");

#endif
    cm_setup_out();
    cm_setup_adc();
}

void cm_setup_out() {
    pinMode(cmT0, OUTPUT);
    pinMode(cmT1, OUTPUT);
    pinMode(cmR0, OUTPUT);
    pinMode(cmR1, OUTPUT);

    digitalWrite(cmT0, HIGH);
    digitalWrite(cmT1, HIGH);
    digitalWrite(cmR0, LOW);
    digitalWrite(cmR1, LOW);
}

void cm_setup_adc() {

    adcAttachPin(cmADC0);
    adcAttachPin(cmADC1);
    adcAttachPin(cmADCT0);
    analogSetPinAttenuation(cmADC0, ADC_11db);
    analogSetPinAttenuation(cmADC1, ADC_11db);
    analogSetPinAttenuation(cmADCT0, ADC_11db);
    analogReadResolution(12);
    i_cm_sqBuffCounter = 0;

    timer = timerBegin(0, 80, true);
    timerAttachInterrupt(timer, &cm_take_sample, true);
    timerAlarmWrite(timer, 250, true);
    timerAlarmEnable(timer);

    fir_lp0.setFilterCoeffs(filter_taps1);
    fir_lp0.getGain();
    fir_lp1.setFilterCoeffs(filter_taps1);
    fir_lp1.getGain();
    cm_calib_adc();
    rmsmax = 0;
}

void cm_calib_adc() {
    delay(1000);
    for (int i = 0; i < cmADCnum; i++) i_cm_calib[i] = 0;
    //    for (int i = 0; i < ci_cm_calibBuff_size; i++) {
    //        adcStart(cmADC0);
    //        i_cm_calib[0] += adcEnd(cmADC0);
    //        adcStart(cmADC1);
    //        i_cm_calib[1] += adcEnd(cmADC1);
    //        //rdebugV("Cal_Meas: %u, %u\n", i_cm_calib[0],  i_cm_calib[1]);
    //    }
    for (int channel = 0; channel < cmADCnum; channel++) {
        for (int i = 0; i < ci_cm_sqBuff_size; i++) {
            i_cm_calib[channel] += i_cm_sqBuff[channel][i];
        }
    }
    for (int i = 0; i < cmADCnum; i++) i_cm_calib[i] /= ci_cm_sqBuff_size;
    rdebugI("Cal_Meas: %f, %f\n", i_cm_calib[0]* 1.95 / 4096.0, i_cm_calib[1]*1.95 / 4096.0);
}

void IRAM_ATTR cm_take_sample() {
    i_cm_sqBuff[0][i_cm_sqBuffCounter] = analogRead(cmADC0);
    i_cm_sqBuff[1][i_cm_sqBuffCounter] = analogRead(cmADC1);
    if (i_cm_sqBuffCounter < ci_cm_sqBuff_size) i_cm_sqBuffCounter++;
    else i_cm_sqBuffCounter = 0;
}

float cm_rms_calc(int channel) {

    //i_cm_sqBuff[channel][i_cm_sqBuffCounter] = fir_lp.processReading(value);
    //calcuate new RMS value use mean of size ci_winduwSize
    float rms = 0;
    for (int i = 0; i < ci_cm_sqBuff_size; i++) {
        float temp = 0;
        //        for (int ii = 0; ii < ci_cm_windowSize; ii++) {
        //            temp += i_cm_sqBuff[channel][i + ii];
        //        }
        //        temp /= ci_cm_windowSize;
        if (channel == 0) temp = fir_lp0.processReading(i_cm_sqBuff[channel][i]) - i_cm_calib[0]; //fir_lp0.processReading(i_cm_sqBuff[channel][i]);
        if (channel == 1) temp = fir_lp1.processReading(i_cm_sqBuff[channel][i]) - i_cm_calib[1]; //fir_lp1.processReading(i_cm_sqBuff[channel][i]);
        rms += temp * temp;
    }
    rms /= ci_cm_sqBuff_size;
    rms = sqrt(rms);
    rms = rms * 8.08 * 3.9 / 4096.0 - 0.021; // - i_cm_sqBuff_Offset[channel];
    return rms * 240.0;
}

void loop() {

#ifndef PRODUCTION // Not in PRODUCTION
	// Time of begin of this loop
	uint32_t timeBeginLoop = millis();
#endif
    int t = analogRead(cmADCT0);
    float temp = 4.1 * t / 4096.0;
    temp = 1212576.05 / (log(2 / (3 * temp) - 2 / 15)*298.15 + 4067) - 273.15;
    float temp0 = cm_rms_calc(0);
    float temp1 = cm_rms_calc(1);
    //if (temp0>rmsmax) rmsmax = temp0;
    
	// Each second

	if ((millis() - mLastTime) >= 1000) {

		// Time

		mLastTime = millis();

		mTimeSeconds++;

#ifndef PRODUCTION // Not in PRODUCTION

		// Debug the time (verbose level) (without shortcut)

		debugV("* Time: %u seconds (VERBOSE)", mTimeSeconds);

		if (mTimeSeconds % 5 == 0) { // Each 5 seconds

			// Debug levels
rdebugV("%i ADC: t= %f, %f (%f), %f (%f)\n", micros(), temp, i_cm_sqBuff[0][i_cm_sqBuffCounter] * 3.6 / 4096.0, temp0, i_cm_sqBuff[1][i_cm_sqBuffCounter] * 3.6 / 4096.0, temp1);
			//debugV("* This is a message of debug level VERBOSE");
			//debugD("* This is a message of debug level DEBUG");
			//debugI("* This is a message of debug level INFO");
			//debugW("* This is a message of debug level WARNING");
			//debugE("* This is a message of debug level ERROR");

			// Call a function

			//foo();

		}
#endif
	}

	////// Services on Wifi

#ifdef USE_ARDUINO_OTA
	// Update over air (OTA)

	ArduinoOTA.handle();
#endif

#ifndef PRODUCTION // Not in PRODUCTION


	Debug.handle();

#endif

	// Give a time for ESP8266

	yield();

	// Show a debug - warning if time of these loop is over 50 (info) or 100 ms (warning)

#ifndef PRODUCTION // Not in PRODUCTION

	uint32_t time = (millis() - timeBeginLoop);

	if (time > 100) {
		debugI("* Time elapsed for the loop: %u ms.", time);
	} else if (time > 200) {
		debugW("* Time elapsed for the loop: %u ms.", time);
	}
#endif

}




// Function example to show a new auto function name of debug* macros

#ifndef PRODUCTION // Not in PRODUCTION

// Process commands from RemoteDebug

void processCmdRemoteDebug() {

	String lastCmd = Debug.getLastCommand();

	if (lastCmd == "bench1") {

		// Benchmark 1 - Printf

		if (Debug.isActive(Debug.ANY)) {
			Debug.println("* Benchmark 1 - one Printf");
		}

		uint32_t timeBegin = millis();
		uint8_t times = 50;

		for (uint8_t i = 1; i <= times; i++) {
			if (Debug.isActive(Debug.ANY)) {
				Debug.printf("%u - 1234567890 - AAAA\n", i);
			}
		}

		if (Debug.isActive(Debug.ANY)) {
			Debug.printf("* Time elapsed for %u printf: %ld ms.\n", times,
					(millis() - timeBegin));
		}

	} else if (lastCmd == "bench2") {

		// Benchmark 2 - Print/println

		if (Debug.isActive(Debug.ANY)) {
			Debug.println("* Benchmark 2 - Print/Println");
		}

		uint32_t timeBegin = millis();
		uint8_t times = 50;

		for (uint8_t i = 1; i <= times; i++) {
			if (Debug.isActive(Debug.ANY)) {
				Debug.print(i);
				Debug.print(" - 1234567890");
				Debug.println(" - AAAA");
			}
		}

		if (Debug.isActive(Debug.ANY)) {
			Debug.printf("* Time elapsed for %u printf: %ld ms.\n", times,
					(millis() - timeBegin));
		}
	}
}
#endif

void connectWiFi() {

	////// Connect WiFi

#ifdef ESP32
	// ESP32 // TODO: is really necessary ?
	WiFi.enableSTA(true);
	delay(1000);
#endif

	// Connect with SSID and password stored

	WiFi.begin();

	// Wait connection

	uint32_t timeout = millis() + 20000; // Time out

	while (WiFi.status() != WL_CONNECTED && millis() < timeout) {
		delay(250);
		Serial.print(".");
	}

	// Not connected yet?

	if (WiFi.status() != WL_CONNECTED) {

		// SmartConfig

		WiFi.beginSmartConfig();

		// Wait for SmartConfig packet from mobile

		Serial.println("connectWiFi: Waiting for SmartConfig.");

		while (!WiFi.smartConfigDone()) {
			delay(500);
			Serial.print(".");
		}

		Serial.println("");
		Serial.println("connectWiFi: SmartConfig received.");

		// Wait for WiFi to connect to AP

		Serial.println("connectWiFi: Waiting for WiFi");

		while (WiFi.status() != WL_CONNECTED) {
			delay(500);
			Serial.print(".");
		}
	}

	// End

	Serial.println("");
	Serial.print("connectWiFi: connect a ");
	Serial.println(WiFi.SSID());
	Serial.print("IP: ");
	Serial.println(WiFi.localIP().toString());

}

#ifdef USE_ARDUINO_OTA

// Initialize o Esp8266 OTA

void initializeOTA() {

	// Port defaults to 8266
	// ArduinoOTA.setPort(8266);
	// Hostname defaults to esp8266-[ChipID]
	// ArduinoOTA.setHostname("myesp8266");
	// No authentication by default
	// ArduinoOTA.setPassword((const char *)"123");

	ArduinoOTA.onStart([]() {

		Serial.println("* OTA: Start");
	});
	ArduinoOTA.onEnd([]() {
		Serial.println("\n*OTA: End");
	});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
		Serial.printf("*OTA: Progress: %u%%\r", (progress / (total / 100)));
	});
	ArduinoOTA.onError([](ota_error_t error) {
		Serial.printf("*OTA: Error[%u]: ", error);
		if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
		else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
		else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
		else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
		else if (error == OTA_END_ERROR) Serial.println("End Failed");
	});
	ArduinoOTA.begin();

}

#endif


/////////// End